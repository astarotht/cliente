# This file is generated automatically. Do not edit.
# Use project properties -> Build -> Qt -> Expert -> Custom Definitions.
TEMPLATE = app
DESTDIR = dist/Debug/GNU-Linux
TARGET = cliente
VERSION = 1.0.0
CONFIG -= debug_and_release app_bundle lib_bundle
CONFIG += debug 
PKGCONFIG +=
QT = core gui widgets
SOURCES += main.cpp src/Comunicacion/Cliente.cpp src/Comunicacion/Servidor.cpp src/Estructuras/Lista.cpp src/Estructuras/Nodo.cpp src/GUI/ComponentesGUI.cpp src/GUI/VentanaPrincipal.cpp src/MODLogica/DataBloques.cpp src/MODLogica/DataBola.cpp src/MODLogica/DataPad.cpp src/MODLogica/Manager.cpp src/MODLogica/TuringMarbas.cpp
HEADERS += src/Estructuras/Lista.hpp src/Estructuras/Nodo.hpp src/GUI/ComponentesGUI.h src/GUI/VentanaPrincipal.h src/MODLogica/DataBloques.h src/MODLogica/DataBola.h src/MODLogica/DataPad.h src/MODLogica/Manager.h src/MODLogica/TuringMarbas.h
FORMS +=
RESOURCES +=
TRANSLATIONS +=
OBJECTS_DIR = build/Debug/GNU-Linux
MOC_DIR = 
RCC_DIR = 
UI_DIR = 
QMAKE_CC = gcc
QMAKE_CXX = g++
DEFINES += 
INCLUDEPATH += ../ServidorMArbas/src/Comunicacion 
LIBS += 
