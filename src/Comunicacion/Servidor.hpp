/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * @File:   Servidor.hpp
 * @Author: elias
 *
 * @Created on 20 de febrero de 2016, 13:46
 */

#ifndef SERVIDOR_HPP
#define SERVIDOR_HPP

#include <iostream>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>

using std::string;

class Servidor {
public:

    Servidor(int, const char*, int);
    int establecerConexionSocket();
    bool enlazarServidor();
    int escucharConexiones(int);
    bool aceptarConexiones();
    bool enviarDatos(string);
    char* recibitDatos();


    virtual ~Servidor();

private:

    int _socket;
    int _cliente;
    int _puerto;
    int _tamDelBuffer;
    bool _cerrado;
    char _buffer[];
    struct sockaddr_in direccionDelServidor;
    socklen_t _tama;
    int _crearCliente;
    const char* _ip;

};

#endif /* SERVIDOR_HPP */

