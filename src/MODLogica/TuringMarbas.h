/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   TuringMarbas.h
 * Author: elias
 *
 * Created on 2 de marzo de 2016, 13:53
 */

#ifndef TURINGMARBAS_H
#define TURINGMARBAS_H

#include <string>
#include <stdlib.h>
#include <QLabel>
#include "../GUI/VentanaPrincipal.h"
using std::string;

class TuringMarbas {
public:
    static TuringMarbas* acceso();
    void leerCinta(string);
    bool mensajeCompleto(string);
    void ejecutarMensaje(string);
    void mensajeDeBolas(string);
    
    virtual ~TuringMarbas();
    string partirString();
private:
    TuringMarbas();
    int parsearValor(string*);
    void actualizarDataBolas(int, int, int);
    static TuringMarbas* _machina;

    int _casilla;
    int _posX;
    int _posY;
    int _bola;
    int _enJuego;
    
    

};

#endif /* TURINGMARBAS_H */

