/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Cliente.cpp
 * Author: elias
 * 
 * Created on 20 de febrero de 2016, 14:10
 */

#include "Cliente.hpp"

Cliente::Cliente(const char* pIpDelServidor, int ppuerto, int pTamBuffer) {

    _ip = pIpDelServidor;
    _puerto = ppuerto;
    _TamBuffer = pTamBuffer;
    _buffer[_TamBuffer];

}

/**
 * @brief Funcíon que crea un cliente. 
 * @return En caso de que el cliente no sea creado se retorna un -1
 */
int Cliente::crearCliente() {

    _direccionDeDestino.sin_family = AF_INET;
    _direccionDeDestino.sin_port = htons(_puerto);
    _direccionDeDestino.sin_addr.s_addr = inet_addr(_ip); //Combierte la direccion a un entero sin signo

    _socket = socket(AF_INET, SOCK_STREAM, 0);
    return _socket;
}

bool Cliente::conectarCliente() {
    int $temporal;
    bzero(&(_direccionDeDestino.sin_zero), 8);
    $temporal = connect(_socket, (struct sockaddr *) &_direccionDeDestino, sizeof (_direccionDeDestino));
    return $temporal != -1;

}

/**
 * @brief Se utiliza para enviar un mensaje al servidor 
 * @param pPalabra string con el mensaje a enviar al servidor
 * @return retorna true si se pudo enviar el mensaje y falso en caso de error
 */
bool Cliente::enviarPalabre(string pPalabra) {

    strcpy(_buffer, pPalabra.c_str());
    send(_socket, _buffer, _TamBuffer, 0);
    return true;

}

/**
 * @brief Se usa para recibir los datos enviados por el servidor
 * @return retorna la palabra que se envio.
 */
string Cliente::recibirPalabra() {

    recv(_socket, _buffer, _TamBuffer, 0);
    return _buffer;

}

Cliente::~Cliente() {

    close(_socket);
    delete(this);

}

