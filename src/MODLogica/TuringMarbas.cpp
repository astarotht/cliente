/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * @File:   TuringMarbas.cpp
 * @Author: elias
 * 
 * @Created on 2 de marzo de 2016, 13:53
 */

#include "TuringMarbas.h"
#include "Manager.h"
TuringMarbas* TuringMarbas::_machina = NULL;

TuringMarbas::TuringMarbas() {
    
}
/**
 * @brief Metodo de acceso al singleton Marbas
 * @return retorna la unica instancia de TuringMarbas
 */
TuringMarbas* TuringMarbas::acceso() {

    if (_machina == NULL) {

        _machina = new TuringMarbas();
        

    }
    return _machina;

}
/**
 * @brief lee el mensaje
 * @param pCinta mensaje que se ha recibido
 */
void TuringMarbas::leerCinta(string pCinta) {

}

bool TuringMarbas::mensajeCompleto(string pCinta) {
    
    if (pCinta.substr(0, 5) == "Draw|" && pCinta.substr(pCinta.length() - 4, 4) == "|end") {

        return true;
    } else
        return false;

}
/**
 * @brief Dice las indicaciones de que debe de hacer la GUI luego de traducir el mensaje
 * @param pCinta mensaje que se debe ejecutar
 */
void TuringMarbas::ejecutarMensaje(string pCinta) {

    if(mensajeCompleto(pCinta)){
        if(pCinta != "v"){
            pCinta = pCinta.substr(5, pCinta.length() - 9);

            if (pCinta.substr(1, 4) == "bola") {

                mensajeDeBolas(pCinta.substr(6, pCinta.length() - 6));
            
            }
        }
    }
}
/**
 * @brief Se encarga de llenar las dataBolas que mas adelante va a leer la GUI
 * @param pMensaje Mensaje previamente cortado y que indica posiciones de bolas en
 * caso contrario el programa tendra un error
 */
void TuringMarbas::mensajeDeBolas(string pMensaje) {

    _casilla = 0;
    _posX = 0;
    _posY = 0;
    _bola = 0;
    _enJuego = -1;
    do {
        _enJuego++;
        _casilla = 0;
        while (true) {

            if (pMensaje[_casilla] == '#') {

                _bola = atoi(pMensaje.substr(0, _casilla).c_str());
                pMensaje = pMensaje.substr(_casilla + 1, (pMensaje.length() - _casilla));
                break;

            }
            _casilla++;
        }

        _casilla = 0;

        while (true) {

            if (pMensaje[_casilla] == '#') {

                _posX = atoi(pMensaje.substr(0, _casilla).c_str());
                pMensaje = pMensaje.substr(_casilla + 1, pMensaje.length() - _casilla);
                break;

            }
            _casilla++;
        }

        _casilla = 0;

        while (true) {

            if (pMensaje[_casilla] == '#' || pMensaje[_casilla] == '*') {

                _posY = atoi(pMensaje.substr(0, _casilla).c_str());
                pMensaje = pMensaje.substr(_casilla + 1 , pMensaje.length() - _casilla);
                break;

            }
            _casilla++;
        }

        Manager::CargarManager()->actualizarDataBola(_bola, _posX, _posY);
    } while (pMensaje != "");


} // retorna final
/**
 * @brief Metodo pensado en caso de que la ventana se enlace al Manager para actualizar la posicion
  actualmente no esta implementado.
 * @param pBola Cual bola se están refiriendo
 * @param pPosX posición en x de la bola
 * @param pPosY posición en y de la bola
 */
void TuringMarbas::actualizarDataBolas(int pBola, int pPosX, int pPosY){
    
    

}

//string  TuringMarbas::numeroaString(int pNumero){
//std::ostringstream $string;
//$string << pNumero;
//return $string.str(); 
//}

TuringMarbas::~TuringMarbas() {

}

