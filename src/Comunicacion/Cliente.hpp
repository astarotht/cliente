/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * @File:   Cliente.hpp
 * @Author: elias
 *
 * @date Created on 20 de febrero de 2016, 14:10
 */

#ifndef CLIENTE_HPP
#define CLIENTE_HPP
#include <iostream>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
using std::string;

class Cliente {
public:
    Cliente(const char*, int, int);
    int crearCliente();
    bool conectarCliente();
    string recibirPalabra();
    bool enviarPalabre(string);
    virtual ~Cliente();
private:

    int _socket;
    int _puerto;
    int _TamBuffer;
    bool _exit;
    char _buffer[];
    const char* _ip;
    struct sockaddr_in _direccionDeDestino;

};

#endif /* CLIENTE_HPP */

