/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   VentanaPrincipal.h
 * Author: elias
 *
 * Created on 25 de febrero de 2016, 15:52
 */

#ifndef VENTANAPRINCIPAL_H
#define VENTANAPRINCIPAL_H

#include <QTextStream>
#include <QApplication>
#include <QObject>
#include <QPushButton>
#include <QFont>
#include <QWidget>
#include <QWindow>
#include <QLabel>
#include <QLineEdit>
#include <string>
#include "../Estructuras/Lista.hpp"
#include "../MODLogica/Manager.h"
#include "../MODLogica/DataBola.h"

using std::string;

class VentanaPrincipal : public QWidget {
    Q_OBJECT

public:
    VentanaPrincipal(bool*);
    bool _banderaDeDatosLlenos();
    bool getBanderaDeDatos();
    string getIp();
    int getPuerto();
    void actualizarestadoDeBola();
signals:
    int repintame(int,int, int);
public slots:
    void refresh();
    void LevantarBanderaDeDatosLlenos();
    void repintar(int, int, int);
    

private:
    bool _mensajeLeido;
    QLabel* _Titulo1;
    QLabel* _Pruebas;
    QLabel* _LabelPuerto;
    QLineEdit* _NumeroDePuerto;
    QLabel* _LabelServerIp;
    QLineEdit* _NumeroDeIp;
    QPushButton* _BotonDeCargar;
    string _ip;
    int _puerto;
    int _largo1;
    int _largo2;
    
    int _x1;
    int _y1;
    bool _DatosLlenos;
    QLabel *_Bolas;
    Lista<DataBola*>* _DataBol;
    DataBola *ptrDataBola;
    QLabel* _ptrLabel;

};

#endif /* VENTANAPRINCIPAL_H */

