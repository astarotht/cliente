/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * @file:   Manager.cpp
 * @author: elias
 * 
 * @date Created on 25 de febrero de 2016, 22:11
 */

#include "Manager.h"
#include "DataBola.h"

Manager* Manager::_ManagerInstancia = NULL;
/**
 * Constructor del manager. Es privado por lo que no tiene acceso a el.
 */
Manager::Manager() {
    
    _DataBolas = new Lista<DataBola*>();
    _DataBolas->insertarFinal(new DataBola());
    _DataBolas->obtenerPosicion(0)->setXY(0,0);
//    _Bloques = new Lista<DataBola*>();
    _banderaCliente = false;
    _recibido = "";
    _temporal = "";

}

/**
 * @brief Se utiliza para conectar el cliente a un servidor
 * @param ppuerto puerto en donde se encuentra el servidor
 * @param pip ip de donde se encuentra el servidor
 */
void Manager::conectarCliente(int ppuerto, string pip) {

    _cliente = new Cliente(pip.c_str(), ppuerto, 1024);
    _cliente->crearCliente();
    _cliente->conectarCliente();
    _banderaCliente = true;

}

/**
 * @brief Se usa para recibir mensajes del servidor
 * @return  retorna los mensajes del servidor
 */
string Manager::recibirMensaje() {

    if (_banderaCliente) {
        _temporal = _cliente->recibirPalabra();
        
                _recibido = _temporal;
                return _temporal;
        }
        else
            return "v";

}
/**
 * @Clase para obtener el socket cliente. Sólo se debe usar para pruebas
 * @return instancia del socket cliente
 */
Cliente* Manager::testingGetCliente() {

    return _cliente;

}

/**
 * @brief Le indica a TuringMarbas que lea el mensaje que llego en el cliente.
  TuringMarbas debe de haberse enlazado previamente y el cliente conectado al 
  servidor
 */
void Manager::lectura(){
    
        TuringMarbas::acceso()->ejecutarMensaje( this->recibirMensaje());
    

}

/**
 * @Actualiza los datos de los parametros que tiene cada databola
 * @param pBola DataBola a cargar datos
 * @param pPosX posición en x de la databola
 * @param pPosY posición en y de la databola
 */
void Manager::actualizarDataBola(int pBola, int pPosX, int pPosY){
    
    
    if (_DataBolas->obtenerLargo() > pBola) {

        _DataBolas->obtenerPosicion(pBola)->setXY(pPosX, pPosY);

    } else {

        _DataBolas->insertarFinal(new DataBola());
        _DataBolas->obtenerPosicion(pBola)->setXY(pPosX, pPosY);
        
    }
    //actualizarVentana();
    
}
/**
 * @brief Retorna el arreglo en donde se encuentran todas las dataBolas
 * @return  retorna el arreglo con las databolas
 */
Lista<DataBola*>* Manager::getDataBolas(){
    
    return _DataBolas;

}

//void Manager::enlazarVentana(VentanaPrincipal* ptrventana){

//    _ventana = ptrventana;

//}
/**
 * @brief Metodo usado para enlazar ventana, e compilador no lo permite por lo
 * que no se ha implementado
 */
void Manager::enlaceVentana(){
    
     //_ventana = pVentana;

}
/**
 * @notes Requiere ventana enlazada para funcionar
 */
void Manager::actualizarVentana(){
    
//    _ventana->actualizarestadoDeBola();

}