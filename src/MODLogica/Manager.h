/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Manager.h
 * Author: elias
 *
 * Created on 25 de febrero de 2016, 22:11
 */

#ifndef MANAGER_H
#define MANAGER_H
#include "../GUI/VentanaPrincipal.h"
#include <string>
#include "TuringMarbas.h"
#include "../Comunicacion/Cliente.hpp"
#include "../MODLogica/DataBola.h"

using std::string;

class Manager {
public:

    void conectarCliente(int, string);
    void lectura();
    void actualizarVentana();
    void enlaceVentana();
    string recibirMensaje();
    Cliente* testingGetCliente();
    void actualizarDataBola(int, int, int);
    Lista<DataBola*>* getDataBolas();

    static Manager* CargarManager() {

        if (_ManagerInstancia == NULL) {

            _ManagerInstancia = new Manager();

        }
        return _ManagerInstancia;

    }

private:
    Manager();

    static Manager* _ManagerInstancia;
    const char* _bufferout;
    string _bufferin;

    Cliente* _cliente;
    bool _banderaCliente;
    string _temporal;
    string _recibido;
    Lista<DataBola*> *_DataBolas;
//    VentanaPrincipal *_ventana;

};

#endif /* MANAGER_H */

