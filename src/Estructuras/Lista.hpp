/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 @file:   Lista.hpp
 @author: elias
 *
 @date Created on 19 de febrero de 2016, 1:42
 */

#ifndef LISTA_HPP
#define LISTA_HPP
#include "Nodo.hpp"
#include <iostream>
#include <string>
using std::cout;
using std::string;

template<typename pTipo>
class Lista {
public:

    /**@brief Contructor de listas dinamicas se debe de especificar el tipo de
    objetos que se van a agregar en ella 
     
     */
    Lista() {

        _largo = 0;

    };

    /**@brief Metodo para saber cuantos elementos contiene actualmente la lista
      @return Retorna un int mayor o igual que 0
     */
    int obtenerLargo() {
        return _largo;
    }

    /**@brief Inserta el objeto al principio de la lista
      @param objeto a introducir en la lista
     */
    void insertarPrincipio(pTipo pDato) {

        _insercionptr = new Nodo<pTipo>(pDato);

        if (_largo != 0) {

            _principioptr->setAnterior(_insercionptr);
            _insercionptr->setSiguiente(_principioptr);
            _principioptr = _insercionptr;

        } else {

            _principioptr = _insercionptr;
            _finalptr = _insercionptr;

        }
        _largo++;

    }

    /**
     * @brief
     * @param pDato
     */
    void insertarFinal(pTipo pDato) {

        _insercionptr = new Nodo<pTipo>(pDato);

        if (_largo != 0) {

            _finalptr->setSiguiente(_insercionptr);
            _insercionptr->setAnterior(_finalptr);
            _finalptr = _insercionptr;

        } else {

            _principioptr = _insercionptr;
            _finalptr = _insercionptr;

        }
        _largo++;

    }

    /**
     * @brief Se inserta un argumento en la función para obtener el dato guardado 
     * en esa posición del arreglo unidimensional
     * @param pArgumento Se inserta un numero menor al largo de la lista para obtener 
     * un objeto de la lista, en caso de colocar un numero mayor se retornada nulo
     * 
     * @return Retorna el objeto de clase pTipo guardada en esta posición
     */
    pTipo obtenerPosicion(int pArgumento) {


        _iteradorptr = _principioptr;
        if (pArgumento <= _largo) {
            for (int _posicion = 0; _posicion < pArgumento; _posicion++) {


                _iteradorptr = _iteradorptr->getSiguiente();

            }


        }

        return _iteradorptr->getDato();

    }

    /**
     * @brief Permite cargar el objeto en una posición especifica de la lista
     * @param pArgumento Se indica la posición en donde se deben ingresar el objeto:
      el primer objeto es 0
     * @param pDato Se inserta el objeto en la lista
     */
    void guardarEnPosicion(int pIndice, pTipo pDato) {

        _insercionptr = new Nodo<pTipo>(pDato);
        _iteradorptr = _principioptr;
        if (pIndice == 0) {
            insertarPrincipio(pDato);
        } else {
            if (pIndice == (_largo - 1)) {

                insertarFinal(pDato);

            } else {
                if (pIndice <= _largo) {
                    for (int _posicion = 0; _posicion < pIndice; _posicion++) {

                        _iteradorptr = _iteradorptr->getSiguiente();

                    }

                    _insercionptr->setAnterior(_iteradorptr->getAnterior());
                    _iteradorptr->getAnterior()->setSiguiente(_insercionptr);
                    _insercionptr->setSiguiente(_iteradorptr);
                    _iteradorptr->setAnterior(_insercionptr);

                }
            }
        }

    }

    /**
     * @Brief Cambia el objeto del indice especificado de la lista por el 
     introducido en los parametros
     * @param pIndice Se indica en que posición de la lista se va a insertar e
     dato
     * @param pDato El objeto que se va a introducir en el nodo
     * @return  El objeto que se encontraba en el nodo
     */
    pTipo cambiarDato(int pIndice, pTipo pDato) {

        _insercionptr = new Nodo<pTipo>(pDato);
        _iteradorptr = _principioptr;
        for (int _posicion = 0; _posicion < pIndice; _posicion++) {

            _iteradorptr = _iteradorptr->getSiguiente();

        }

        pTipo $temporal = _iteradorptr->getDato();
        _iteradorptr->setDato(pDato);
        return $temporal;

    }

    //Lista(const Lista& orig);

    virtual ~Lista() {
/**
        for (int _posicion = 0; _posicion < _largo; _posicion++) {
            _iteradorptr = _finalptr;
            _finalptr = _finalptr->getAnterior();
            _iteradorptr->~Nodo();
        }
*/

    };
private:

    Nodo<pTipo> *_principioptr = 0;
    Nodo<pTipo> *_finalptr = 0;
    Nodo<pTipo> *_iteradorptr = 0;
    Nodo<pTipo> *_insercionptr = 0;
    int _largo =  0;

};

#endif /* LISTA_HPP */

