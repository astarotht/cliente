/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * @File:   VentanaPrincipal.cpp
 * @Author: elias
 * 
 * @date Created on 25 de febrero de 2016, 15:52
 */

#include "VentanaPrincipal.h"
#include "src/MODLogica/Manager.h"
#include "src/MODLogica/DataBola.h"


/**
 * @brief Clase que representa la ventana del cliente
 */
VentanaPrincipal::VentanaPrincipal(bool *_mensajeLeido) : QWidget() {

    //this->windowTitleChanged("Cliente");
    
    _Bolas = new QLabel("bola", this);
    _DatosLlenos = false;
    _Titulo1 = new QLabel("Inserte los datos, requeridos para conectar con el servidor", this);
    _LabelPuerto = new QLabel("Inserte el numero de puerto aquí", this);
    _NumeroDePuerto = new QLineEdit(this);
    _LabelServerIp = new QLabel("Inserte el numero de ip en que se encuentra el servidor", this);
    _NumeroDeIp = new QLineEdit(this);
    _BotonDeCargar = new QPushButton("Conectar Cliente", this);
    
    
    this->resize(400, 200);
    _Titulo1->move(10, 10);
    _LabelPuerto->move(10, 50);
    _NumeroDePuerto->move(10, 80);
    _LabelServerIp->move(10, 110);
    _NumeroDeIp->move(10, 140);
    _BotonDeCargar->move(150, 170);

    QObject::connect(this->_BotonDeCargar, SIGNAL(clicked()), this, SLOT(LevantarBanderaDeDatosLlenos()));
    QObject::connect(this, SIGNAL(repintame(int,int,int)), this, SLOT( repintar(int,int,int) ));
    this->show();

}
/**
 * @brief En este metodo se deja de mostrar las indicaciones, cajas de texto,
 se envia el numero de puerto y la ip al manager,ademas, se agranda el tamaño de 
 * la ventana
 */
void VentanaPrincipal::LevantarBanderaDeDatosLlenos() {
    _ip = _NumeroDeIp->displayText().toStdString();
    _puerto = _NumeroDePuerto->displayText().toInt();

    Manager::CargarManager()->conectarCliente(_puerto, _ip);
    this->resize(800, 600);
    _NumeroDePuerto->setVisible(false);
    _NumeroDeIp->setVisible(false);
    _Titulo1->setVisible(false);
    _LabelPuerto->setVisible(false);
    _LabelServerIp->setVisible(false);
    _BotonDeCargar->setVisible(false);
    _DatosLlenos = true;
    
    
}

void VentanaPrincipal::refresh(){
    
    this->repaint();

}
/**
 * @brief Se obtiene la bandera datos llenos
 * @return booleano falso si el usuario no ha ingresado datos, verdadero en caso contrario
 */
bool VentanaPrincipal::getBanderaDeDatos() {
    return _DatosLlenos;
}
/**
 * @brief retorna un string con la ip que agrego el usuario
 * @return retorna la ip a la que el usuario desea conectarse
 */
string VentanaPrincipal::getIp() {

    return _ip;

}
/**
 * @brief Retorna el puerto al que el usuario desea conectarse
 * @return un int con el valor del puerto a conectar
 */
int VentanaPrincipal::getPuerto() {

    return _puerto;

}
/**
 * @brief envia una señal para indicar que se debe actualizar el estado de la bola
 */
void VentanaPrincipal::actualizarestadoDeBola() {    

}
/**
 * @brief repinta la bola indicada en los parametros
 * @param ppos bola que se debe repintar
 * @param px nueva posición en x
 * @param py nueva posición en y
 */
void VentanaPrincipal::repintar(int ppos ,int px, int py){
    
    //ptrDataBola = Manager::CargarManager()->getDataBolas()->obtenerPosicion(0);
    _ptrLabel = _Bolas;
    _ptrLabel->move(px, py);
    //this->repaint();
    QTextStream(stdout) << "Desde Ventana QT" << px << py << "\n";

}