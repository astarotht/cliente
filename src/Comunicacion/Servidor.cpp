/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * @File:   Servidor.cpp
 * @Author: elias
 * 
 * @date Created on 20 de febrero de 2016, 13:46
 */

#include "Servidor.hpp"
#include <iostream>
#include <fcntl.h>
using std::string;

/**
 * @brief Constructor utilizado para instanciar el objeto servidor
 * @param pPuerto Especifica el puerto que se va abrir en el equipo para
 * recibir comunicaciones
 * @param pTamBuffer Especifica el tamaño del buffer de lectura. Se recomienda 1024
 */
Servidor::Servidor(int pPuerto, const char* pIpDelServidor, int ptamDelBuffer) {

    _puerto = pPuerto;
    _ip = pIpDelServidor;
    _tamDelBuffer = ptamDelBuffer;
    _buffer[ptamDelBuffer];

}

/**
 * Esta función crea un nuevo socket retorna un dato tipo int
 * @return Retorna -1 en caso de que el socket no pueda ser creado. En caso contrario
 * genera un integer que es usado para referenciar el socket que se creo
 */

int Servidor::establecerConexionSocket() {

    direccionDelServidor.sin_family = AF_INET;
    direccionDelServidor.sin_addr.s_addr = inet_addr(_ip);
    direccionDelServidor.sin_port = htons(_puerto);
    _socket = socket(AF_INET, SOCK_STREAM, 0);
    //fcntl(_cliente, F_SETFL, O_NONBLOCK); //bloqueante;
    return _socket;

}

/**
 * @Brief Enlaza las lllamadas del sistema con el socket
 * @return false en caso que no se logre y true en caso que si se pueda enlazar
 */
bool Servidor::enlazarServidor() {

    int $tempo = 0;
    $tempo = bind(_socket, (struct sockaddr*) &direccionDelServidor, sizeof (direccionDelServidor));
    if ($tempo < 0) {

        return false;

    }

    _tama = sizeof (direccionDelServidor);
    return true;
}

/**
 * @brief Escucha para saber si existe algún cliente que desea conectarse
 * @param pConexiones cantidad de conexiones que se esperan
 * @return retorna un int
 */
int Servidor::escucharConexiones(int pcantidadDeConexiones) {

    int listeni = listen(_socket, pcantidadDeConexiones);
    return listeni;

}

/**
 * @brief Se aceptan las conexiones de los clientes.
 * @return  returna falso es caso de error y true en caso de establecerse la conexión
 */
bool Servidor::aceptarConexiones() {


    _cliente = accept(_socket, (struct sockaddr *) &direccionDelServidor, &_tama);
    if (_cliente < 0) {
        return false;
    } else
        return true;

}

/**
 * @brief Función utilizada para enviar datos al socket conectado.
 * @param pDatos String que se va a enviar, no puede ser mayor a 1024 caracteres.
 * recordar que el final de un string cuenta como un caracter \0
 */
bool Servidor::enviarDatos(string pDatos) {

    if ((pDatos.length() + 1) < _tamDelBuffer) {


        strcpy(_buffer, pDatos.c_str());
        send(_cliente, _buffer, _tamDelBuffer, 0);
        return true;

    } else {

        return false;

    }



}

/**
 * @Muestra los datos que han sido enviados al servidor
 * @return Retorna un string del tamaño previamente definido en el constructor
 */
char* Servidor::recibitDatos() {

    recv(_cliente, _buffer, _tamDelBuffer, 0);
    return _buffer;

}

Servidor::~Servidor() {
}

