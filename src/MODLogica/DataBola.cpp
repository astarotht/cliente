/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * @File:   DataBola.cpp
 * @Author: elias
 * 
 * @date Created on 2 de marzo de 2016, 21:41
 */

#include "DataBola.h"
/**
 * @brief constructor por defecto no recibe parametros
 */
DataBola::DataBola() {
    _x = 0;
    _y = 0;
}
/**
 * @brief retorna la posicion en x de la bola
 * @return se retorna un int con el valor en x de la bola 
 */
int DataBola::getX(){
    
    return _x;

}

/**
 * @brief retorna la posicion en y de la bola
 * @return se retorna un int con el valor en y de la bola 
 */
int DataBola::getY(){
    
    return _y;

}
/**
 * @brief Coloca cuales deben ser la posición en X y Y de la bola
 * @param pX parametro que define la ubicación en x
 * @param pY parametro que define la ubicación en x
 */
void DataBola::setXY(int pX, int pY){
    
    _x = pX;
    _y = pY;

}


/**
 * @brief destructor de la bola;
 */
DataBola::~DataBola() {
}

