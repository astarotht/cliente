/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   main.cpp
 * Author: elias
 *
 * Created on 25 de febrero de 2016, 15:13
 */

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include  <unistd.h>
#include <iostream>
#include <QApplication>
#include "src/GUI/VentanaPrincipal.h"
#include "src/MODLogica/Manager.h"
#include "src/Comunicacion/Servidor.hpp"
#include "src/MODLogica/DataBola.h"
using std::cout;

DataBola *_bola;
pthread_mutex_t _bloqueo;
bool _mensajeLeido = false;
QApplication *app;
int _x;
int _y;
VentanaPrincipal* _ventanaCliente;
string _pruebas;
pthread_t _mensaje, _GUI, _application;

/**
 * @Funcion encargada de leer y ejecutar los mensajes recibidos es se ejecuta
 * constantemente en un hilo
 * @param arg 
 * @return 
 */
void *funcion01(void *arg) {

    while (true) {
        

        _pruebas = Manager::CargarManager()->recibirMensaje();
        cout << _pruebas << '\n';
        
            if (_pruebas != "v" && _pruebas.substr(0,4) == "Draw"){//_pruebas != "v") {
//                pthread_mutex_lock(&_bloqueo);
                cout << _pruebas << "***" << '\n';
                Manager::CargarManager()->lectura();
                _mensajeLeido = true;
                
                _bola = Manager::CargarManager()->getDataBolas()->obtenerPosicion(0);
                _x = _bola->getX();
                _y = _bola->getY();
//                _ventanaCliente->repintar(0, _x, _y);
                //_mensajeLeido = true;
//                _ventanaCliente->repintame(0, _x, _y);
//                pthread_mutex_unlock(&_bloqueo);
                
            }
        
        
        usleep(10);
        
        
    }
    return NULL;
}

/**
 * @brief Función en que la ventana cliente se redibuja y actualiza el estado actual
 * @param arg
 * @return 
 */
void *funcion02(void *arg) {

    try {
        while (true) {

            if (_mensajeLeido) {
                _bola = Manager::CargarManager()->getDataBolas()->obtenerPosicion(0);
                _x = _bola->getX();
                _y = _bola->getY();
                _ventanaCliente->repintar(0, _x, _y);
                _mensajeLeido = true;
                //_ventanaCliente->repintame(0, _x, _y);
            }
            
            cout << "pintando";
            usleep(50);
            
        }


    } catch (int i) {

    }
}

void *funcion03(void *arg) {
    //while(true){
        
        //pthread_join(_GUI, NULL);
        //pthread_join(_mensaje, NULL);
        
        
    //}
    return NULL;
}

/**
 * @brief función main en donde se ejecuta el programa y se activan los demas hilos
 */

int main(int argc, char *argv[]) {

    app = new QApplication(argc, argv);
    // initialize resources, if needed
    // Q_INIT_RESOURCE(resfile);

    Manager::CargarManager();



    _ventanaCliente = new VentanaPrincipal(&_mensajeLeido);
    //Manager::CargarManager()->enlazarVentana(_ventanaCliente);
    //if (pthread_create(&_GUI, NULL, funcion02, NULL)) {
//        printf("Error creando el hilo de De Universo");
  //      abort();
//    }

    if (pthread_create(&_mensaje, NULL, funcion01, NULL)) {
        printf("Error creando el hilo de De Universo");
        abort();
    }
    
    if (pthread_create(&_application, NULL, funcion02, NULL)) {
            printf("Error creando el hilo de De Universo");
            abort();
        }
        
    app->exec();
}