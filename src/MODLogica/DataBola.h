/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DataBola.h
 * Author: elias
 *
 * Created on 2 de marzo de 2016, 21:41
 */

#ifndef DATABOLA_H
#define DATABOLA_H

class DataBola {
public:
    DataBola();
    int getX();
    int getY();
    void setXY(int, int);
    DataBola(const DataBola& orig);
    virtual ~DataBola();
private:
    int _x;
    int _y;
    int _x1;
    int _y1;
};

#endif /* DATABOLA_H */

