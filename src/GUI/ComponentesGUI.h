/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ComponentesGUI.h
 * Author: elias
 *
 * Created on 2 de marzo de 2016, 14:20
 */

#ifndef COMPONENTESGUI_H
#define COMPONENTESGUI_H

class ComponentesGUI {
public:
    static ComponentesGUI* acceso();
    virtual ~ComponentesGUI();
private:
    ComponentesGUI();
    static ComponentesGUI *_fabrica;

};

#endif /* COMPONENTESGUI_H */

